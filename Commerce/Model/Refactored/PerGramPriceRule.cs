﻿namespace Commerce.Model.Refactored
{
	public class PerGramPriceRule : IPriceRule
	{
		public bool IsMatch(OrderItem orderItem)
		{
			return orderItem.Sku.StartsWith("WEIGHT");
		}

		public decimal CalculatePrice(OrderItem orderItem)
		{
			return orderItem.Quantity * 4m / 1000;
		}
	}
}