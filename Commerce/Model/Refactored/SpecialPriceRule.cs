﻿namespace Commerce.Model.Refactored
{
	public class SpecialPriceRule : IPriceRule
	{
		public bool IsMatch(OrderItem orderItem)
		{
			return orderItem.Sku.StartsWith("SPECIAL");
		}

		public decimal CalculatePrice(OrderItem orderItem)
		{
			decimal total = 0m;
			// $0.40 each, 3 for $1.00
			total += orderItem.Quantity * 0.4m;
			var setsOfThree = orderItem.Quantity / 3;
			total -= setsOfThree * .2m;
			return total;
		}
	}
}