﻿using System;

namespace LSP.CodeSmells
{
	public abstract class Base
	{
		public abstract void Method1();

		public abstract void Method2();
	}

	public class Child : Base
	{
		public override void Method1()
		{
			throw new NotImplementedException();
		}

		public override void Method2()
		{
			// Do stuff
		}
	}
}
