﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LSP.Refactored
{
	public abstract class Shape
	{
		public abstract double Area();
	}

	public class Rectangle : Shape
	{
		public virtual int Height { get; set; }
		public virtual int Width { get; set; }

		public override double Area()
		{
			return Height * Width;
		}
	}

	public class Square : Shape
	{
		public int SideLength { get; set; }

		public override double Area()
		{
			return SideLength * SideLength;
		}
	}

	[TestClass]
	public class CalculateAreaShouldReturn
	{
		[TestMethod]
		public void SixFor2X3Rectangle()
		{
			var myRectangle = new Rectangle { Height = 2, Width = 3 };
			Assert.AreEqual(6, myRectangle.Area());
		}

		[TestMethod]
		public void NineFor3X3Square()
		{
			var mySquare = new Square() { SideLength = 3 };
			Assert.AreEqual(9, mySquare.Area());
		}

		[TestMethod]
		public void TwentyFor4X5ShapeFromRectangle()
		{
			Shape newRectangle = new Rectangle { Width = 4, Height = 5 };
			Assert.AreEqual(20, newRectangle.Area());
		}

		[TestMethod]
		public void TwentyFor4X5ShapeFromRectangleAnd9For3X3Square()
		{
			var shapes = new List<Shape>
			{
				new Rectangle {Height = 4, Width = 5},
				new Square {SideLength = 3}
			};

			var areas = new List<double>();

			foreach (Shape shape in shapes)
			{
				areas.Add(shape.Area());
			}

			Assert.AreEqual(20, areas[0]);
			Assert.AreEqual(9, areas[1]);
		}
	}
}
