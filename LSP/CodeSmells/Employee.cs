﻿using System;
using System.Collections.Generic;

namespace LSP.CodeSmells
{
	public class Employee
	{
		public void PrintEmployee()
		{
			
		}
	}

	public class Manager : Employee
	{
		public void PrintManager()
		{
			
		}
	}


	public class Program
	{
		private Printer _printer;

		public void Process()
		{
			foreach (var emp in Employees)
			{
				if (emp is Manager)
				{
					_printer.PrintManager(emp as Manager);
				}
				else
				{
					_printer.PrintEmployee(emp);
				}
			}
		}

		public IEnumerable<Employee> Employees { get; set; }
	}

	internal class Printer
	{
		public void PrintManager(Manager manager)
		{
			throw new NotImplementedException();
		}

		public void PrintEmployee(Employee emp)
		{
			throw new NotImplementedException();
		}
	}
}
