﻿using System;

namespace ISP
{
	public interface IMachine
	{
		void Print();
		void Staple();
		void Scan();
		void Copy();
	}

	#region Xerox

	public class Xerox : IMachine
	{
		public void Print()
		{
			// Print the document
		}

		public void Staple()
		{
			// Staple the document
		}

		public void Scan()
		{
			// Scan the document
		}

		public void Copy()
		{
			// Copy the document
		}
	}

	#endregion

	#region Copier

	public class Copier : IMachine
	{
		public void Print()
		{
			throw new NotImplementedException();
		}

		public void Staple()
		{
			throw new NotImplementedException();
		}

		public void Scan()
		{
			throw new NotImplementedException();
		}

		public void Copy()
		{
			// Make a photocopy of the document
		}
	}

	#endregion

	#region Scanner

	public class Scanner : IMachine
	{
		public void Print()
		{
			throw new NotImplementedException();
		}

		public void Staple()
		{
			throw new NotImplementedException();
		}

		public void Scan()
		{
			// Scan the document
		}

		public void Copy()
		{
			throw new NotImplementedException();
		}
	}

	#endregion
}
