using Commerce.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Commerce
{
	[TestClass]
	public class CartTotalShouldReturn
	{
		private Model.Cart _cart;

		[TestInitialize]
		public void Setup()
		{
			_cart = new Model.Cart();
		}

		[TestMethod]
		public void ZeroWhenEmpty()
		{
			Assert.AreEqual(0, _cart.TotalAmount());
		}

		[TestMethod]
		public void FiveWithOneEachItem()
		{
			_cart.Add(new OrderItem { Quantity = 1, Sku = "EACH_WIDGET" });
			Assert.AreEqual(5m, _cart.TotalAmount());
		}

		[TestMethod]
		public void TwoWothHalfKiloWeightItem()
		{
			_cart.Add(new OrderItem { Quantity = 500, Sku = "WEIGHT_PEANUTS" });
			Assert.AreEqual(2m, _cart.TotalAmount());
		}


		[TestMethod]
		public void EightyCentsWithTwoSpecialItems()
		{
			_cart.Add(new OrderItem { Quantity = 2, Sku = "SPECIAL_CANDYBAR" });
			Assert.AreEqual(0.8m, _cart.TotalAmount());
		}

		[TestMethod]
		public void TwoDolarsWithSixSpecialItems()
		{
			_cart.Add(new OrderItem { Quantity = 6, Sku = "SPECIAL_CANDYBAR" });
			Assert.AreEqual(2m, _cart.TotalAmount());
		}
	}
}
