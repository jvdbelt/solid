﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIP
{
	public static class Program
	{
		public static void Main()
		{
			var time = new MorningCalendar();
			var c = new ExplicitDependencies(time);
			Console.WriteLine(c.Hello("Jeroen"));
		}
	}
}
