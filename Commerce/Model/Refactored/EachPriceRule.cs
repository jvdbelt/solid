﻿namespace Commerce.Model.Refactored
{
	public class EachPriceRule : IPriceRule
	{
		public bool IsMatch(OrderItem orderItem)
		{
			return orderItem.Sku.StartsWith("EACH");
		}

		public decimal CalculatePrice(OrderItem orderItem)
		{
			return orderItem.Quantity * 5m;
		}
	}
}