﻿using System.Collections.Generic;

namespace Commerce.Model
{
	public class Cart
	{
		public List<OrderItem> Items { get; }

		public string CustomerEmail { get; set; }

		public Cart()
		{
			Items = new List<OrderItem>();
		}

		public void Add(OrderItem orderItem)
		{
			Items.Add(orderItem);
		}

		public decimal TotalAmount()
		{
			var total = 0m;
			foreach (var orderItem in Items)
			{
				if (orderItem.Sku.StartsWith("EACH"))
				{
					total += orderItem.Quantity * 5m;
				}
				else if (orderItem.Sku.StartsWith("WEIGHT"))
				{
					// Quantity is in grams, price is per kg
					total += orderItem.Quantity * 4m / 1000;
				}
				else if (orderItem.Sku.StartsWith("SPECIAL"))
				{
					// 0.40 each, 3 for 1.00
					total += orderItem.Quantity * 0.4m;
					var setsOfThree = orderItem.Quantity / 3;
					total -= setsOfThree * .2m;
				}
				// more rules are coming!
			}
			return total;
		}
	}
}
