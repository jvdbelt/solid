﻿namespace Commerce.Model.Refactored
{
	public interface IPriceRule
	{
		bool IsMatch(OrderItem orderItem);
		decimal CalculatePrice(OrderItem orderItem);
	}
}