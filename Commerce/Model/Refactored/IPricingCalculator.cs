﻿namespace Commerce.Model.Refactored
{
	public interface IPricingCalculator
	{
		decimal CalculatePrice(OrderItem orderItem);
	}
}