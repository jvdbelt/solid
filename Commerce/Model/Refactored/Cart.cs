﻿using System.Collections.Generic;

namespace Commerce.Model.Refactored
{
	public class Cart
	{
		private readonly IPricingCalculator _pricingCalculator;
		public List<OrderItem> Items { get; }

		public string CustomerEmail { get; set; }

		public Cart() : this(new PricingCalculator())
		{
		}

		public Cart(IPricingCalculator pricingCalculator)
		{
			_pricingCalculator = pricingCalculator;
			Items = new List<OrderItem>();
		}

		public void Add(OrderItem orderItem)
		{
			Items.Add(orderItem);
		}

		public decimal TotalAmount()
		{
			var total = 0m;
			foreach (var orderItem in Items)
			{
				total += _pricingCalculator.CalculatePrice(orderItem);
				// more rules are coming!
			}
			return total;
		}
	}
}
