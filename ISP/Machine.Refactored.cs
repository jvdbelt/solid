﻿namespace ISP.Refactored
{
	public interface IPrintable
	{
		void Print();
	}

	public interface IStapler
	{
		void Staple();
	}

	public interface IScanner
	{
		void Scan();
	}

	public interface ICopier
	{
		void Copy();
	}


	#region Xerox

	public class Xerox : IPrintable, IScanner, ICopier, IStapler
	{
		public void Print()
		{
			// Print the document
		}

		public void Staple()
		{
			// Staple the document
		}

		public void Scan()
		{
			// Scan the document
		}

		public void Copy()
		{
			// Copy the document
		}
	}

	#endregion

	#region Copier

	public class Copier : ICopier
	{
		public void Copy()
		{
			// Make a photocopy of the document
		}
	}

	#endregion

	#region Scanner

	public class Scanner : IScanner
	{
		public void Scan()
		{
			// Scan the document
		}
	}

	#endregion
}
