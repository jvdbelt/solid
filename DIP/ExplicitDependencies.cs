﻿using System;

namespace DIP
{
	public class ExplicitDependencies
	{
		private readonly ICalendar _dateTime;

		public ExplicitDependencies(ICalendar dateTime)
		{
			_dateTime = dateTime;
		}

		public string Hello(string name)
		{
			if (_dateTime.Now.Hour < 12) return "Good morning, " + name;
			if (_dateTime.Now.Hour < 18) return "Good afternoon, " + name;
			return "Good evening, " + name;
		}
	}

	public interface ICalendar
	{
		DateTime Now { get; }
	}

	internal class MorningCalendar : ICalendar
	{
		public DateTime Now
		{
			get { return new DateTime(2000, 1, 1, 10, 0, 0); }
		}
	}

	public class SystemCalendar : ICalendar
	{
		public DateTime Now
		{
			get { return DateTime.Now; }
		}
	}
}
