﻿using System.Collections.Generic;
using System.Linq;

namespace Commerce.Model.Refactored
{
	public class PricingCalculator : IPricingCalculator
	{
		private readonly List<IPriceRule> _pricingRules;

		public PricingCalculator()
		{
			_pricingRules = new List<IPriceRule>();
			_pricingRules.Add(new EachPriceRule());
			_pricingRules.Add(new PerGramPriceRule());
			_pricingRules.Add(new SpecialPriceRule());
		}

		public decimal CalculatePrice(OrderItem orderItem)
		{
			return _pricingRules.First(x => x.IsMatch(orderItem)).CalculatePrice(orderItem);
		}
	}
}